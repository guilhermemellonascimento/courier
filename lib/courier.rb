require 'courier/version'
require 'courier/asset'
require 'courier/content_parser'
require 'courier/integrations'

module Courier
  class << self
    def send(key, data)
      asset = Asset.find_by!(key: key)

      body = parse_content(asset, data)
      options = data.merge!(body: body)

      Integrations.new(asset.integration).send(options)
    end

    private

    def parse_content(asset, data)
      ContentParser.new(asset, data).call
    end
  end
end
