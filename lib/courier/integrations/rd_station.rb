require 'sidekiq'
require 'courier/worker'
require 'httparty'

module Courier
  class RdStation < Worker
    include HTTParty

    headers 'Content-Type' => 'application/json'
    base_uri 'https://www.rdstation.com.br/api/1.3'

    def perform(options = {})
      options.merge!(token_rdstation: ENV['TOKEN_RDSTATION'])

      self.class.post('/conversions', options)
    end
  end
end
