require 'spec_helper'
require 'sidekiq/testing'

describe Courier do
  Struct.new('Asset', :name, :key, :content_type, :integration, :content)

  data = {
    name: 'Paul',
    email: 'paul@gmail.com.br',
    subject: 'Welcome!'
  }

  context '.send' do
    it "raises an error when an invalid key is passed" do
      allow(Courier::Asset).to receive(:find_by).and_return(nil)

      expect {
        Courier.send('invalid-key', data)
      }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it "sends amazon_ses integration based on a valid key" do
      allow(Courier::Asset).to receive(:find_by).and_return(
        Struct::Asset.new('Welcome email', 'welcome', 'html', 'amazon_ses', 'Olá {{name}}, seja bem vindo!')
      )

      expect {
        Courier.send('welcome', data)
      }.to change(Courier::AmazonSES.jobs, :size).by(1)
    end

    it "sends rd_station integration based on a valid key" do
      content = "{
        \"email\": \"{{email}}\",
        \"name\": \"{{name}}\",
        \"identifier\": \"{{identifier}}\",
        \"url\": \"{{url}}\"
      }"

      allow(Courier::Asset).to receive(:find_by).and_return(
        Struct::Asset.new('Sign Up', 'sign_up', 'json', 'rd_station', content)
      )

      expect {
        Courier.send('sign_up', data)
      }.to change(Courier::RdStation.jobs, :size).by(1)
    end
  end
end
