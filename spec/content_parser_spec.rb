require 'spec_helper'

describe Courier::ContentParser do
  Struct.new('Asset', :name, :key, :content_type, :integration, :content)

  let(:asset) { Struct::Asset.new('Welcome email', 'welcome', 'html', 'amazon_ses', 'Olá {{name}}, seja bem vindo!')}
  let(:data) {
    { name: 'Joe foo' }
  }

  it 'returns the parsed content' do
    parsed_content = Courier::ContentParser.new(asset, data).call
    expect(parsed_content).to eq('Olá Joe foo, seja bem vindo!')
  end
end
