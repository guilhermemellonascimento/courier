require 'mustache'

module Courier
  class ContentParser
    attr_reader :asset, :data

    def initialize(asset, data)
      @asset = asset
      @data = data
    end

    def call
      if asset.content_type == 'json'
        JSON.parse(Mustache.render(asset.content, data))
      else
        Mustache.render(asset.content, data)
      end
    end
  end
end
