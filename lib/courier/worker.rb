module Courier
  class Worker
    include Sidekiq::Worker
    sidekiq_options queue: 'courier'
  end
end
