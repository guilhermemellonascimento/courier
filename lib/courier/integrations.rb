require 'courier/integrations/amazon_ses'
require 'courier/integrations/rd_station'

module Courier
  class Integrations
    attr_reader :integration

    def initialize(integration)
      @integration = integration
    end

    def send(options)
      case integration.to_sym
      when :amazon_ses
        AmazonSES.perform_async(options)
      when :rd_station
        RdStation.perform_async(options)
      end
    end
  end
end
