require 'sidekiq'
require 'aws-sdk'
require 'courier/worker'

module Courier
  class AmazonSES < Worker
    SENDER = 'noreply@qconcursos.com'

    def perform(options = {})
      ses = Aws::SES::Client.new(region: 'us-east-1')

      ses.send_email(
        destination: {
          to_addresses: [
            options['email']
          ]
        },
        message: {
          body: {
            html: {
              data: options['body']
            }
          },
          subject: {
            data: options['subject']
          }
        },
        source: SENDER
      )
    end
  end
end
