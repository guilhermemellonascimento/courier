# Courier

Courier allows you to send messages through integrations such as: AmazonSES, Sendgrid, RdStation.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'courier'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install courier

## Usage

All integrations are `Sidekiq workers` that execute asynchronously.

The content of an asset can be parsed using a hash of data like this:

content | data | result
--- | --- | ---
`Welcome {{name}}!` | `{ name: 'Foo Bar' }` | `Welcome Foo Bar!`

### AmazonSES:

To make API call to AmazonSES you need do configure credentials. The `aws-sdk` searches for the following ENV variables:

`ENV['AWS_ACCESS_KEY_ID']` and `ENV['AWS_SECRET_ACCESS_KEY']`

**Sending AmazonSES message**

```ruby
user = {
  login: 'foo',
  email: 'foo@gmail.com'
}

data = {
  name: 'Foo Bar',
  subject: 'Welcome!'
}

Courier.send('welcome', user, data)
```
